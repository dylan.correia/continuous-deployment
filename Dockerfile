
FROM composer:1.9.0 AS composer
WORKDIR /app
COPY composer.json .
COPY composer.lock .
ARG APP_ENV=dev
RUN if [ "$APP_ENV" = "prod" ]; then composer install --no-scripts ; else composer install ; fi
##

FROM node:12.9.1-slim AS node
WORKDIR /app
COPY package.json .
COPY yarn.lock .
COPY webpack.config.js .
COPY ./assets ./assets
ARG APP_ENV=dev
RUN yarn && mkdir public && if [ "$APP_ENV" = "prod" ]; then yarn encore production ; else yarn encore dev ; fi

###
ARG APP_SECRET=5169803ee557722e512bbd45a2092863
ARG POSTGRES_DB=database
ARG POSTGRES_PASSWORD=s3cr3t
ARG POSTGRES_USER=user
ARG DATABASE_URL=postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@postgres:5432/${POSTGRES_DB}
ARG MAILER_URL=smtp://mailhog:1025

ENV APP_SECRET=$APP_SECRET
ENV POSTGRES_DB=$POSTGRES_DB
ENV POSTGRES_PASSWORD=$POSTGRES_PASSWORD
ENV POSTGRES_USER=$POSTGRES_USER
ENV DATABASE_URL=$DATABASE_URL
ENV MAILER_URL=$MAILER_URL

FROM php:7.2-apache
WORKDIR /var/www/project
COPY --chown=www-data:www-data . /var/www/project
COPY --chown=www-data:www-data --from=composer /app/vendor /var/www/project/vendor
COPY --chown=www-data:www-data --from=node /app/public/build /var/www/project/public/build
RUN sed -i "s|DocumentRoot /var/www/html|DocumentRoot /var/www/project/public|" /etc/apache2/sites-available/000-default.conf && \
    a2enmod rewrite && mkdir var && chown www-data:www-data var
    
#update apache port at runtime for Heroku
ENTRYPOINT []
CMD sed -i "s/80/$PORT/g" /etc/apache2/sites-enabled/000-default.conf /etc/apache2/ports.conf && docker-php-entrypoint apache2-foreground
